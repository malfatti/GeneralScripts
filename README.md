# GeneralScripts

## Dependencies

### Mandatory
- Linux
- Bash
- Python > 3.6

### Python
- Numpy
- pyQt5

The environment variable `GENSCRIPTS` is expected to be set. You can add it to `~/.bashrc`, or `~/.profile`, or wherever your desktop environment searches for exported variables:
```bash
export GENSCRIPTS=~/Git/GeneralScripts
```
changing the path to where you find appropriate.

