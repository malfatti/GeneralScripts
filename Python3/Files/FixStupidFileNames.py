#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 20180724
@license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/GeneralScripts
"""

import os
import unicodedata
from argparse import ArgumentParser

Parser = ArgumentParser()
Parser.add_argument('--files', nargs='*', help='Files to rename')
Parser.add_argument('--skipconfirm', default='no', help='Move files without confirmation [DANGER]')
Args = Parser.parse_args()

Files = Args.files
NewFiles = []
for File in Files:
    if '/' in File: Path = '/'.join(File.split('/')[:-1])
    else: Path = '.'

    New = File.split('/')[-1]

    for C in ['"',"'",'!','@','#','$','%','&','*','+','=',';',':','?',',']:
        if C in New: New = New.replace(C,'')

    for C in ['(', ')', '[', ']', '{', '}', '<', '>', '|']:
        if C in New: New = New.replace(C,'_')

    if ' ' in New:
        New = New.title()
        New = New.replace(' ', '')

    New = ''.join((c for c in unicodedata.normalize('NFD', New) if unicodedata.category(c) != 'Mn'))

    if New[0] == New[0].lower():
        New = list(New)
        New[0] = New[0].upper()
        New = ''.join(New)

    if '.' in New:
        New = '.'.join(New.split('.')[:-1]) + '.' + New.split('.')[-1].lower()

    New = Path+'/'+New
    NewFiles.append(New)

if Args.skipconfirm.lower() not in ('y','yes'):
    print('Files will be renamed as:')
    for F,File in enumerate(Files):
        print(f'    {File} | {NewFiles[F]}')

    Ans = input('Proceed? [y/N] ')
    if Ans.lower() not in ('y', 'yes'):
        raise KeyError('Aborted.')

for F,File in enumerate(Files):
    if File != NewFiles[F]: os.rename(File, NewFiles[F])


