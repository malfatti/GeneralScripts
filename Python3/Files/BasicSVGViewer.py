#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 20200720
@license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/GeneralScripts
"""

import sys
from PyQt5.QtWidgets import QApplication
from PyQt5 import QtSvg

App = QApplication(sys.argv)
Widget = QtSvg.QSvgWidget(sys.argv[1])
Widget.show()

sys.exit(App.exec_())
