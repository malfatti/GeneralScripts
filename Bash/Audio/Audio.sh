#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200801
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

# Audio control

#%% Level 0

function AudioCompressor {
    Prefix=OggConverted
    Rate=192000

    while [[ $# -gt 1 ]]; do
        Key="$1"
        case $Key in
            --file) File="$2" ;;
            --prefix) Prefix="$2" ;;
            --rate) Rate="$2" ;;
        esac
        shift
    done

    readarray -t Lines < $File
    echo > Log

    echo "Total number of files: ${#Lines[@]}"
    if [ ${#Lines[@]} == 0 ]; then return 0; fi

    for L in $(seq 1 ${#Lines[@]}); do
        Line=${Lines[L-1]}
#         readarray -d '/' -t LineSplit < <(echo "$Line")
        Folder="$Prefix"/"${Line%/*}"

        mkdir -p $Folder &> /dev/null

        Line=${Line// /\?}
        Name=${Line//\?/_}; Name=${Name##*\/}; Name=${Name%.*}
        echo "Compressing $Folder/$Name to ogg"...
        ffmpeg -hide_banner -n -i $Line -acodec libopus -b:a 192000 -vn $Folder/"$Name".ogg  &>> Log
    done

    echo 'Done.'
}


JackCtl() {
    while [[ $# -gt 1 ]]; do
        Key="$1"

        case $Key in
            --path) Path="$2" ;;
            --rt) RT="$2" ;;
            --priority) Prio="$2" ;;
            --card) Card="$2" ;;
            --rate) Rate="$2" ;;
            --periods) Periods="$2" ;;
            --periodsize) PeriodSize="$2" ;;
            --bridge) Bridge="$2" ;;
        esac
        shift
    done

    echo "Jack path: "$Path""
    echo "Jack options: jackd -"$RT" -P"$Prio" -dalsa -dhw:"$Card" -r"$Rate" -p"$PeriodSize" -n"$Periods""
    echo ""

    cp $GENSCRIPTS/Bash/Audio/Jack/Jack ~/.asoundrc
    "$Path"jackd -"$RT" -P"$Prio" -dalsa -dhw:"$Card" -r"$Rate" -p"$PeriodSize" -n"$Periods" &

    sleep 2

    if [ ${Bridge,,} == true ]; then
        cp $GENSCRIPTS/Bash/Audio/Jack/Jack-ALSA ~/.asoundrc

        Catch=30000
        ResamplingRate=48000
        ResamplingQuality=1

        echo "Building output bridge..."
        echo "alsa_out options:  -j ALSAInput -dALSAOutput1 -f "$Catch" -q "$ResamplingQuality" -r "$ResamplingRate" -p "$PeriodSize" -n "$Periods""
        echo ""

        "$Path"alsa_out -j ALSAInput -d"ALSAOutput1" -f "$Catch" -q "$ResamplingQuality" -r "$ResamplingRate" -p "$PeriodSize" -n "$Periods" &
        #"$Path"alsa_out -j ALSAInput -dALSAOutput1 &

        echo "Building input bridge..."
        echo "alsa_in options: -j ALSAOutput -dALSAInput1 -f "$Catch" -q "$ResamplingQuality" -r "$ResamplingRate" -p "$PeriodSize" -n "$Periods""
        echo ""

        "$Path"alsa_in -j ALSAOutput -dALSAInput1 -f "$Catch" -q "$ResamplingQuality" -r "$ResamplingRate" -p "$PeriodSize" -n "$Periods" &
        #"$Path"alsa_in -j ALSAOutput -dALSAInput1 &

        sleep 2

        echo "Connecting bridge to output..."
        "$Path"jack_connect ALSAOutput:capture_1 system:playback_1
        "$Path"jack_connect ALSAOutput:capture_2 system:playback_2

        echo "Connecting bridge to input..."
        "$Path"jack_connect system:capture_1 ALSAInput:playback_1
        "$Path"jack_connect system:capture_2 ALSAInput:playback_2
    fi

    echo ""
}


#%% Level 1

AudioCtl () {
    #JackPath="/home/malfatti/Software/Programs/Jack1/bin/"
    JackPath="/usr/bin/"
    killall jackd &>> /dev/null
    sleep 1
    # cp $GENSCRIPTS/Bash/Audio/ALSA/PCHVol.sh $GENSCRIPTS/Bash/Audio/ALSA/VolControl.sh

    if [ "${1,,}" == alsa ]; then
        echo 'Jack is not running.' > ~/.log/Audio.log

        if [ "${2,,}" == pch ]; then
            cp $GENSCRIPTS/Bash/Audio/ALSA/PCH ~/.asoundrc
        elif [ "${2,,}" == hdmi ]; then
            cp $GENSCRIPTS/Bash/Audio/ALSA/HDMI ~/.asoundrc
        elif [ "${2,,}" == spdif ]; then
            cp $GENSCRIPTS/Bash/Audio/ALSA/SPDIf ~/.asoundrc
        fi

    elif [ "${1,,}" == jack ]; then
        echo '' > ~/.log/Audio.log

        if [ "${2,,}" == pch ]; then
            JackCtl --path "$JackPath" --rt r --card PCH --rate 96000 --periodsize 512 --periods 4 --priority 19 --bridge false &>> ~/.log/Audio.log

        elif [ "${2,,}" == pchbridge ]; then
            JackCtl --path "$JackPath" --rt r --card PCH --rate 48000 --periodsize 512 --periods 4 --priority 19 --bridge true &>> ~/.log/Audio.log

        elif [ "${2,,}" == pchrt ]; then
            JackCtl --path "$JackPath" --rt R --card PCH --rate 192000 --periodsize 512 --periods 4 --priority 89 --bridge false &>> ~/.log/Audio.log

        elif [ "${2,,}" == pchrtbridge ]; then
            JackCtl --path "$JackPath" --rt R --card PCH --rate 192000 --periodsize 512 --periods 4 --priority 89 --bridge true &>> ~/.log/Audio.log

        elif [ "${2,,}" == hdmi ]; then
            JackCtl --path "$JackPath" --rt r --card HDMI,3 --rate 48000 --periodsize 512 --periods 4 --priority 19 --bridge false &>> ~/.log/Audio.log

        elif [ "${2,,}" == hdmibridge ]; then
            JackCtl --path "$JackPath" --rt r --card HDMI,3 --rate 48000 --periodsize 512 --periods 4 --priority 19 --bridge true &>> ~/.log/Audio.log

        elif [ "${2,,}" == nxrt ]; then
            JackCtl --path "$JackPath" --rt R --card NX --rate 96000 --periodsize 256 --periods 4 --priority 89 --bridge false &>> ~/.log/Audio.log

        elif [ "${2,,}" == usbpre2rt ]; then
            JackCtl --path "$JackPath" --rt R --card USBPre2 --rate 192000 --periodsize 512 --periods 4 --priority 89 --bridge true &>> ~/.log/Audio.log
        fi

    elif [ "${1,,}" == info ]; then
        if [ "${2,,}" == jacklog ]; then
            less  ~/.log/Audio.log
        elif [ "${2,,}" == rtthreads ]; then
            ps -eLo comm,rtprio,pri,class,pid,%cpu,%mem,user --sort rtprio
        else
            less ~/.asoundrc
        fi
    else
        echo "Usage: AudioCtl [alsa | jack ] <subdevice>"
        echo "ALSA subdevices: PCH, HDMI, SPDIf"
        echo "Jack subdevices:"
        echo "    PCH, PCHBridge, PCHRT, PCHRTBridge,"
        echo "    HDMI, NXRT, USBPre2RT"
        echo ""
    fi
}


#%% Level 2

AudioStart () {
    KernelName="$(uname -r)";
    if [ "${KernelName#*rt}" != "$KernelName" ]; then
        AudioCtl Jack PCHRTBridge
    else
        AudioCtl Jack PCHBridge
    fi
}

