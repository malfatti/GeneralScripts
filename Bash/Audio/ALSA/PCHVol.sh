#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

if [ "${1,,}" == up ]; then
    amixer -c PCH sset Master,0 1+
elif [ "${1,,}" == down ]; then
    amixer -c PCH sset Master,0 1-
elif [ "${1,,}" == toggle ]; then
    amixer -c PCH sset Master,0 toggle
else
    echo "Usage: up, down or toggle"
fi
