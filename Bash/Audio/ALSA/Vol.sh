#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

if [ "${2,,}" == up ]; then
    amixer -c "$1" sset Master,0 1+
elif [ "${2,,}" == down ]; then
    amixer -c "$1" sset Master,0 1-
elif [ "${2,,}" == toggle ]; then
    amixer -c "$1" sset Master,0 toggle
else
    echo "Usage: ${0} <card> <up | down | toggle>"
fi
