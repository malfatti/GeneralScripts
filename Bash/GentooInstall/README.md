# Installing gentoo

## Preparation

First, boot a USB system and edit GentooInstall.conf.
Then, run the command:

```bash
# bash GentooInstall.sh
```

This will leave you chrooted inside the new system.


## Inside the new system

```bash
# source /etc/profile && bash /GentooInstall_ChRoot.sh
```

