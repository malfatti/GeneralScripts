#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

# Check displays with
#     $ xrandr | grep "normal left inverted right" | cut -d' ' -f1
Laptop=eDP
HDMI=HDMI-A-0

# WALLPAPERS=$CALIGOPATH/Ashiok/Wallpapers  # Set in ~/.Paths
StateFile="$LOGPATH/DisplayState"
State="$(cat $StateFile)"

if xrandr | grep "$HDMI connected"; then
    if [ $State == "OutOff" ]; then
        xrandr --output "$HDMI" --auto
        xrandr --output "$HDMI" --right-of "$Laptop"
        feh --bg-scale --randomize $WALLPAPERS/*
        echo "Extended" > $StateFile

    elif [ $State == "Extended" ]; then
        xrandr --output "$HDMI" --auto
        xrandr --output "$HDMI" --same-as "$Laptop"
        feh --bg-scale --randomize $WALLPAPERS/*
        echo "Mirror" > $StateFile

    # elif [ $State == "Mirror" ]; then
        # xrandr --output "$HDMI" --auto
        # xrandr --output "$Laptop" --off
        # echo "InOff" > $StateFile

    else
        xrandr --output "$Laptop" --auto
        xrandr --output "$HDMI" --off
        feh --bg-scale --randomize $WALLPAPERS/*
        echo "OutOff" > $StateFile
    fi
else
    xrandr --output "$HDMI" --off
    feh --bg-scale --randomize $WALLPAPERS/*
    echo "OutOff" > $StateFile
fi
