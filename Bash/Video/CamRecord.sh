#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

Date=$(date +%Y%m%d-%H%M%S)

ffmpeg -f v4l2 \
       -framerate 25 \
       -video_size 1280x720 \
-i /dev/video1 Video-$Date.mp4
