#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

## Change default display
Display="$1"
State="$2"
StateArg="$3"

Displays="$(xrandr | grep "normal left inverted right" | cut -d' ' -f1 | tr "\n" ' ')"
Displays="${Displays::-1}"

Help() {
    echo ""
    echo "Usage:"
    echo "    "${0##*/}" ["${Displays// / \| }"] [on | off | right | left | mirror | mode] [arg]"
    echo ""
}

if [[ ! "${Displays,,}" =~ "${Display,,}" ]]; then Help; fi

case "${State,,}" in
	"on")
		xrandr --output "$Display" --auto
		sleep 1
		feh --bg-scale --randomize $WALLPAPERS/*
		;;

	"off")
		xrandr --output "$Display" --off
		;;

	"right")
		xrandr --output "$Display" --right-of "$StateArg"
		sleep 1
		feh --bg-scale --randomize $WALLPAPERS/*
		;;

	"mirror")
		xrandr --output "$Display" --auto
        xrandr --output "$Display" --same-as "$StateArg"
        sleep 1
        feh --bg-scale --randomize $WALLPAPERS/*
        ;;

	"mode")
		xrandr --output "$Display" --auto
        xrandr --output "$Display" --mode "$StateArg"
		sleep 1
		feh --bg-scale --randomize $WALLPAPERS/*
		;;

	*)
        Help
esac
