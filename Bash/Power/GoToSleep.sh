#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

echo "Locking screen..."; slock &
echo "Stopping Jack...";
killall jackd &> /dev/null; killall alsa_in &> /dev/null
echo "Suspending..."; loginctl suspend

echo "Waking..."
echo "Starting Jack...";
bash ~/Software/Git/Malfatti/SciScripts/Bash/Audio/ALSA/SoundAutostart.sh &> /dev/null
echo "Awake."
