#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

## Power off storage device

Device="$@"
echo "Syncing disks..."
sync

echo "Unmounting all partitions..."
for Part in "$Device"?; do udisksctl unmount --block-device $Part; done

echo "Powering off..."
udisksctl power-off --block-device "$Device"
