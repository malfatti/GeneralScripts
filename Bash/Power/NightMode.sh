#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20201003
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

Night=2850
Day=5500

if [ "${1,,}" == on ]; then
    sudo /home/malfatti/Caligo/Ashiok/Documents/Scripts/Bash/BackLight.sh --set 10
    redshift -P -O $Night
elif [ "${1,,}" == off ]; then
    redshift -P -O $Day
else
    echo 'Usage:'
    echo '    NightMode [on|off]'
    echo ''
fi
