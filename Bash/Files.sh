#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20201014
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

CheckSHA512() {
    while getopts fs Option; do
        case "${Option}" in
            f)
                File=${OPTARG}
            ;;
            s)
                SHA=${OPTARG}
            ;;
        esac
    done

    SHACS=$(grep -A 1 -i sha512 "$SHA" | awk '{ print $4 }' | sed -n 1p)
    FileCS=$(sha512sum "$File" | awk '{ print $1 }')

    if [ SHACS == FileCS ]; then
        echo "File $File is valid"
    else
        echo 'File is NOT ok!!'
        echo "File = $FILECS"
        echo "SHA  = $SHA"
    fi
}


function ConvertDoc {
    File="${1%.*}"
    echo '\usepackage[left=3cm,right=2cm,top=3cm,bottom=2cm]{geometry}' > WaitForIt
    echo '\usepackage{setspace} \spacing{1.45}' >> WaitForIt
    echo '\usepackage{indentfirst}' >> WaitForIt
    echo '\usepackage{hyperref}' >> WaitForIt
    echo '\setlength\parindent{24pt}' >> WaitForIt
    pandoc "$1" -H WaitForIt -V subparagraph -V fontsize=12pt -V colorlinks -o "$File".pdf
    rm WaitForIt
}


FixPGFs() {
    for f in *pgf; do
        echo "$f";
        sed -i 's/\\rmfamily//g' "$f";
        sed -i 's/\\sffamily//g' "$f";
        sed -i 's/\\setmainfont{Arial}//g' "$f";
    done
}


function FixStupidFileNamesSH {
    # Remove some special characters and change <> {} [] () to _
    # DEPRECATED. Much better python implementation at
    # https://gitlab.com/malfatti/GeneralScripts/-/blob/master/Python3/Files/FixStupidFileNames.py

    for i in *; do
        new=$(echo ${i} | \
                sed -e "s/[\"\!\@\#\$\%\&\*\+\=\,\;\:\?\[{\']//g" | \
                tr 'ç' c | \
                tr 'Ç' C | \
                tr '(' _ | \
                tr ')' _ | \
                tr '[' _ | \
                tr ']' _ | \
                tr '{' _ | \
                tr '}' _ | \
                tr '<' _ | \
                tr '>' _)
        mv "$i" "$new"
    done

    ## Lowercase file names
    for i in *; do
        mv "$i" "$(echo $i|tr A-Z a-z)"
    done

    ## Capitalize every word of file names
    for i in *; do
        new=$(echo "$i" | sed -e 's/^./\U&/g; s/ ./\U&/g')
        mv "$i" "$new"
    done

    ## Remove spaces on filenames
    for i in *; do
        new=$(echo "$i" | sed 's/ //g')
        mv "$i" "$new"
    done
}

function LaTex {
    File="${1%.*}"
    if [ "${2,,}" == bib ]; then Bib=True; else Bib=False; fi

    mkdir ."$File"
    /usr/bin/latex -interaction=nonstopmode -output-directory ."$File" "$1"

    if [ $Bib == True ]; then
        TEXMFOUTPUT=".$File:" bibtex ."$File"/"$File"
        /usr/bin/latex -interaction=nonstopmode -output-directory ."$File" "$1"
    fi

    /usr/bin/latex -interaction=nonstopmode -output-directory ."$File" "$1"
}

function pdflatex {
    File="${1%.*}"
    if [ "${2,,}" == bib ]; then Bib=True; else Bib=False; fi

    mkdir ."$File"
    /usr/bin/pdflatex -output-directory ."$File" "$1" #&>> ."$File"/"$File".log

    if [ $Bib == True ]; then
        TEXMFOUTPUT=".$File:" bibtex ."$File"/"$File"
        /usr/bin/pdflatex -output-directory ."$File" "$1" #&>> ."$File"/"$File".log
        /usr/bin/pdflatex -output-directory ."$File" "$1" #&>> ."$File"/"$File".log
    fi

    /usr/bin/pdflatex -output-directory ."$File" "$1" #&>> ."$File"/"$File".log
    mv ."$File"/"$File".pdf .
}

function LaTex2rtf {
    echo "Converting to rtf..."
    LaTex "$1" > /dev/null
    latex2rtf -d0 -b "$1".bbl "$1"
    echo "Done."
}

function LaTex2txt {
    echo "Preparing $@.dvi..."
    LaTex "$1" > /dev/null

    echo "Converting to text and removing formatting..."
    catdvi --debug=0 ."$1"/"$1".dvi > "$1"1.txt
    cat "$1"1.txt | echo -n $(sed 's/^$/STARTPARA/') | sed 's/STARTPARA/\n\n/g' > "$1".txt

    echo "Cleaning..."
    rm "$1"1.txt

    echo "Done."
}

function LaTex2docx {
    echo "Converting to rtf..."
    LaTex2rtf "$1" > /dev/null
    libreoffice --headless --invisible --norestore --convert-to docx "$1".rtf
    rm "$1".rtf
    echo "Done."
}

function LaTex2Doc {
    if [ "${1,,}" == --help ]; then
        echo "Usage: LaTex2Doc [File name w/o .tex] [docx | rtf | txt]"
    fi

    if [ $2 == 'txt' ]; then
        LaTex2txt "$1"
    fi

    if [ $2 == 'rtf' ]; then
        LaTex2rtf "$1"
    fi

    if [ $2 == 'docx' ]; then
        LaTex2docx "$1"
    fi
}


function MkPatch {
    File1=$1
    File2=$2
    Out=$3

    diff -crB "$File1" "$File2" > "$Out"
}

# function mvp {
    # Files=( "$1" )
    # source=$1
    # target=${2%/}
    # for f in "${!Files[@]}"; do echo "${Files[$f]}"; done
    # echo "$target/$(dirname -- "$source")"
    # echo "$source" "$target/$(dirname -- "$source")/"
    # # mkdir -p -- "$target/$(dirname -- "$source")"
    # # mv -- "$source" "$target/$(dirname -- "$source")/"
# }

function OrganizeFileNames {
    if [ "${1,,}" == --help ]; then
        printf "
    Usage:
        OrganizeFileNames [prefix] [extension of files to be renamed]

    This will organize files by order in folder. Ex:

        $ ls
        abcde.png    lfjkdls.txt    ndkfwiueh.txt    qrstu.png    wkjhfr.txt

        $ OrganizeFileNames MyFiles txt
        Files that will be modified:
        lfjkdls.txt    ndkfwiueh.txt    wkjhfr.txt
        Are you sure you want to continue? [y/N] y

        Done renaming files. Log recorded to file ./RenameLog .

        $ ls
        abcde.png    MyFiles01.txt    MyFiles02.txt    MyFiles03.txt    qrstu.png

    Note that only files with the selected extension will be modified.

    "
        exit 0
    fi

    echo "Files that will be modified:"
    ls *."$2"
    echo "Are you sure you want to continue? [y/N]"

    read Ans

    if [ \( "${Ans,,}" == y \) -o \( "${Ans,,}" == yes \) ]; then
        Number=1
        for File in *."$2"; do
            aa=$(printf "%02d" "$Number")
            mv "$File" "$1""$aa"."${File##*.}";
            echo ""$File" moved to "$1""$aa"."${File##*.}"" >> RenameLog;
            ((Number++))
        done
        echo "Done renaming files. Log recorded to file ./RenameLog ."
    else
        echo "Aborted."
    fi
}


PDFCompress() {
    gs -dNOPAUSE -dQUIET -dBATCH \
        -sDEVICE=pdfwrite \
        -dCompatibilityLevel=1.4 \
        -dPDFSETTINGS=/prepress \
        -sOutputFile="${1/.pdf}"-Compressed.pdf "$1"
}


RGBToCMYK() {
    # Convert RGB pdfs to CMYK
    gs -dSAFER -dBATCH -dNOPAUSE -dNOCACHE \
        -sDEVICE=pdfwrite \
        -sColorConversionStrategy=CMYK \
        -sColorConversionStrategyForImages=CMYK \
        -dProcessColorModel=/DeviceCMYK \
        -dPDFSETTINGS=/prepress \
        -sOutputFile="${1/.pdf}"-CMYK.pdf "$1"
}

function TrimSpaces {
    sed -i 's/[[:blank:]]*$//' "$1"
}

