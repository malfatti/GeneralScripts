#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20201028
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

FoldersDL=(
    Audio/Music/
)

for f in "${Folders[@]}"; do
    echo "Syncing ~/"$f" to OxiddaGolem/"$f" ..."
    rsync -avP --delete ~/"$f" /run/media/malfatti/OxiddaGolem/"$f" &>> SyncLog
done
echo "Done."
