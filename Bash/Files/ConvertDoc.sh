#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

File="${1%.*}"
echo '\usepackage[left=3cm,right=2cm,top=3cm,bottom=2cm]{geometry}' > WaitForIt
echo '\usepackage{setspace} \spacing{1.45}' >> WaitForIt
echo '\usepackage{indentfirst}' >> WaitForIt
echo '\setlength\parindent{24pt}' >> WaitForIt
pandoc "$1" -H WaitForIt -V subparagraph -V fontsize=12pt -V colorlinks -o "$File".pdf
rm WaitForIt
