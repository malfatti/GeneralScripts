#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

i="$1"

## Remove some special characters and change <> {} [] () to _
new=`echo ${i} | \
    sed -e "s/[\"\!\@\#\$\%\&\*\+\=\,\;\:\?\[{\']//g" | \
    tr 'ç' c | \
    tr 'Ç' C | \
    tr '(' _ | \
    tr ')' _ | \
    tr '[' _ | \
    tr ']' _ | \
    tr '{' _ | \
    tr '}' _ | \
    tr '<' _ | \
    tr '>' _`

## Lowercase file names
new="$(echo $new|tr A-Z a-z)"

## Capitalize every word of file names
new=`echo "$new" | sed -e 's/^./\U&/g; s/ ./\U&/g'`

## Remove spaces on filenames
new=`echo "$new" | sed 's/ //g'`

echo "$new"

