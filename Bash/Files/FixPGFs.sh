#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

for f in *pgf; do
    echo "$f";
    sed -i 's/\\rmfamily//g' "$f";
    sed -i 's/\\sffamily//g' "$f";
    sed -i 's/\\setmainfont{Arial}//g' "$f";
done

