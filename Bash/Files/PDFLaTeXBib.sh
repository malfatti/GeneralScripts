#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

File="${1%.*}"

mkdir ."$File" &>> /dev/null
pdflatex --output-directory ."$File" "$1" #&>> ."$File"/"$File".log
TEXMFOUTPUT=".$File:" bibtex ."$File"/"$File"
pdflatex --output-directory ."$File" "$1" #&>> ."$File"/"$File".log
pdflatex --output-directory ."$File" "$1" #&>> ."$File"/"$File".log
mv ."$File"/"$File".pdf .

