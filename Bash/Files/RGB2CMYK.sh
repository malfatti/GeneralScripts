#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

## Convert RGB pdfs to CMYK
gs -dSAFER -dBATCH -dNOPAUSE -dNOCACHE \
	-sDEVICE=pdfwrite \
	-sColorConversionStrategy=CMYK \
	-sColorConversionStrategyForImages=CMYK \
	-dProcessColorModel=/DeviceCMYK \
	-dPDFSETTINGS=/prepress \
	-sOutputFile="${1/.pdf}"-CMYK.pdf "$1"
