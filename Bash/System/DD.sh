#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

Mode="$1"
In="$2"
Out="$3"
BS="$4"

if [ "${Mode,,}" == backup ]; then
    sudo dd if="$In" conv=sync,noerror bs="$BS" | gzip -c  > "$Out"
elif [ "${Mode,,}" == restore ]; then
    sudo gunzip -c "$In" | sudo dd of="$Out" bs="$BS" status=progress
else
    echo "Usage:"
    echo "    DD [backup|restore] <input> <output> <bs>"
fi
