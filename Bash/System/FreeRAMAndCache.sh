#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

echo "Clearing RAM cache..."
sudo su -c "echo 1 >'/proc/sys/vm/drop_caches'"
echo "Clearing Swap..."
sudo swapoff -a && sudo swapon -a
echo "Clearing RAM cache again..."
sudo su -c "echo 1 >'/proc/sys/vm/drop_caches'"
echo "Done."
