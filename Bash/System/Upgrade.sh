#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20201004
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

OptsW='-vautDN --with-bdeps=y --jobs=1 --quiet --complete-graph'

Sync() {
    echo "Syncing... ==============================================================="
    emerge -v --sync
}

UpgradePortage() {
    echo "Upgrading portage... ====================================================="
    emerge -auN1 portage
}

UpgradeKernel() {
    echo "Upgrading kernel... ======================================================"
    emerge -auN1 gentoo-sources
    echo ''
    echo 'Time to eselect/compile kernel and update grub!'
}

RebuildKernelModules() {
    echo "Rebuilding kernel modules... ============================================="
    emerge -a @module-rebuild
}

UpgradeWorld() {
    echo "Upgrading @world... ======================================================"
    emerge $OptsW @world
}

CleanDeps() {
    echo "Cleaning dependencies... ================================================="
    emerge -ac
}

RebuildPreserved() {
    echo "Rebuilding preserved libraries... ========================================"
    emerge -a @preserved-rebuild
}

CleanAll() {
    echo "Cleaning... =============================================================="
    revdep-rebuild
    eclean-dist --destructive
    # eix-test-obsolete
}

WhileRun() {
    Ans='y'
    while [ \( "${Ans,,}" == y \) -o \( "${Ans,,}" == yes \) ]; do
        asusctl led-pow-1 -l false && asusctl led-mode pulse
        "$@"
        asusctl led-pow-1 -l false && asusctl led-mode rain
        echo "Re-run ${1}? [y/N]"; read Ans
    done
}

Order=(
    Sync UpgradePortage UpgradeKernel RebuildKernelModules
    UpgradeWorld
    CleanDeps RebuildPreserved CleanAll
)

swapoff -a
for f in "${Order[@]}"; do WhileRun "$f"; done
swapon -a

echo 'REMOVE --EXCLUDE ARGUMENTS FROM MAKE.CONF!!!'
echo "Done. ===================================================================="


