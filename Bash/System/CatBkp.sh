#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

Mode=$1; In=$2; Out=$3

if [ "${Mode,,}" == backup ]; then
    User=$(whoami)
    Cmd=$(echo "cat $In | gzip -c > $Out")
    echo "Packing ${Out##*/} ..."
    sudo su -c "$Cmd"
    echo "Setting Ownership..."
    sudo chown $User $Out
    echo "Done."
elif [ "${Mode,,}" == restore ]; then
    Cmd=$(echo "gunzip -c $In > $Out")
    sudo su -c "$Cmd"
else
    echo "Usage:"
    echo "    CatBkp [backup|restore] <input> <output>"
fi
