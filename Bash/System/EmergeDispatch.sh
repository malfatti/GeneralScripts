#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20201004
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

EmergeDispatch() {
    Repeat=True

    while [ $Repeat == True ]; do
        emerge "$@"
        if [ $? -eq 0 ]; then Repeat=False; else Repeat=True; fi
        dispatch-conf
    done
}

