#!/bin/bash
# -*- coding: utf-8 -*-
#
# @author: T. Malfatti <malfatti@disroot.org>
# @date: 20200720
# @license: GNU GPLv3 <https://gitlab.com/malfatti/GeneralScripts/raw/master/LICENSE>
# @homepage: https://gitlab.com/Malfatti/GeneralScripts

#SBATCH --time=7-0:0
#SBATCH --cpus-per-task=32
#SBATCH --hint=compute_bound
#SBATCH --exclusive
#SBATCH --qos=qos2

#python3 $SCRIPTSPATH/Python3/Analysis/Units.py
#for f in $SCRATCH_GLOBAL/Analysis/Data/*/*/; do
Here=$(pwd)
cd "$1"
$HOME/.local/bin/klusta Data_Exp00.prm --cluster-only
cd "$Here"
